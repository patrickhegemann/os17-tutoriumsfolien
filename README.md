# Tutoriumsfolien #

Folien f�r Tutorium 7 zur Vorlesung Betriebssysteme am KIT im Wintersemester 2017/18

Die Folien k�nnt ihr einfach mit `git clone https://patrickhegemann@bitbucket.org/patrickhegemann/os17-tutoriumsfolien.git` im Terminal herunterladen
und mit `git pull` (im entsprechenden Ordner) synchronisieren. Das setzt voraus, dass ihr git installiert habt. Alternativ k�nnt ihr auch in diesem Repository bei "Source" die einzelnen Foliens�tze herunterladen.

Falls euch auf den Folien Fehler auffallen, meldet euch gerne bei mir.